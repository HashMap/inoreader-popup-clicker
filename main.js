function waitForAddedNode(params) {
    new MutationObserver(function(mutations) {
        var el = document.getElementsByClassName(params.clazz);
        if (el[0]) {
            this.disconnect();
            params.done(el);
        }
    }).observe(params.parent || document, {
        subtree: !!params.recursive,
        childList: true,
    });
}

function clickIt() {
  var link = document.querySelector('.adb_right_link_passive a')
  if(link !== null) {
    link.click()
  }
}

var dialog = document.querySelector('.inno_dialog_modal')

if(dialog != null) {
  clickIt()
} else {
  waitForAddedNode({
    clazz: 'inno_dialog_modal',
    recursive: true,
    done: function(el) {
      clickIt()
    }
  })
}
